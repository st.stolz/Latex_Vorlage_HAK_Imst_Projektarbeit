# Verwendung des Templates

## 1) Latex-Umgebung installieren

### Direkte Installation von LaTeX

Es wird die TEXLive Distribution empfohlen, die für alle wichtigen Betriebssysteme erhältlich ist. Die ISO-Image-Variante beinhaltet alle Pakete und ist für die Offline-Installation auf mehreren Geräten sinnvollerweise in Betracht zu ziehen. 

### Verwendung von Docker

Nutzer, die eine funktionierende Docker Installation ihr Eigen nennen können als wesentlich elegantere Variante einen Docker Container zum kompilieren verwenden. Siehe dazu entsprechende Anleitung unter "2) Editor installieren".

## 2) Editor installieren

### TeXstudio als Editor für Latex

TeXstudio als Fork von Texmaker ist eine smarter Editor bzw.  Entwicklungsumgebung für das Erstellen von Latex-Dokumenten.

### Visual Studio Code Docker Variante

Folgende vscode Extensions installieren:

* Docker
* Remote-Containers
* LaTeX Workshop

Außerdem muss geprüft werden, ob docker auf docker hub angemeldet ist (CLI: `docker login` ausführen).

Nun in vscode "View" - "Command Palette ..." ausführen und "Remote-Containers: Open Folder in Container ..." wählen. Im folgenden Dialog den Ordner "Latex Vorlage HAK Imst Projektarbeiten" wählen. Der Rest sollte automatisch ablaufen. 

Für die Verwendung der "Latex Vorlage IT Kolleg Imst.tex" ist der Einfachste Weg, diese in "Latex Vorlage HAK Imst.tex" umzubenennen.

## 3) Template verwenden (TeXStudio)

In TeXstudio (oder einem anderen Latex-Editor) die Datei  Latex Vorlage HAK Imst.tex öffnen. Das ist die Root-Datei, in der alle anderen Kapitel etc. eingebunden werden. Die Datei kann in TeXstudio über den grünen Doppelpfeil in der Menüleiste kompiliert werden. Es entsteht eine PDF-Datei, gleichen Namens im selben Ordner.

Wenn die Kapitel bearbeitet werden sollen, dann einfach die Kapitel-Dateien in TeXStudio öffnen und bearbeiten. Weitere Kapitel können analog zu den bestehenden Beispielen hinzugefügt werden.

## 4) Git-scm und Tortoise-Git für die Versionierung

Für die Versionierung von Dokumenten empfiehlt es sich, 
online Repositories wie Gitlab oder Bitbucket zu nutzen. 
Diese unterstützen im Gegensatz zu Github auch gratis 
private Repositories. Am Client-Desktop helfen dabei die 
Werkzeuge wie der Git-Client und die Explorer-Integration 
wie Tortoise-Git die Versionierungen zu verwalten. Wer 
lieber in der Kommandozeile ala Linux auf Windows mit Git 
arbeitet, dem sei der Cmder Consolen-Emulator empfohlen. Dieser ist nach Eingewöhnung und durch Unterstützung verschiedener Kommandos und Shortcuts eine hilfreiche Unterstützung. 

